import React from "react";
import propTypes from "prop-types";
import "../css/Backdrop.css";
const Backdrop = ({ backdropClick }) => {
  return <div className="backdrop" onClick={backdropClick}></div>;
};

Backdrop.propTypes = {
  backdropClick: propTypes.func,
};
export default Backdrop;
