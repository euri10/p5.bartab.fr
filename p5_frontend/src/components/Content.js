import PropTypes from "prop-types";
import React, { Fragment } from "react";
import { useParams } from "react-router";
import contentDataMap from "../contentData";
import "../css/Content.css";
import CodeContent from "./CodeContent";
import SketchContent from "./SketchContent";

const Content = () => {
  let params = useParams();
  return (
    <div className="content">
      <Fragment>
        <SketchContent
          content={contentDataMap}
          sketchName={params.sketchName}
        />
        <CodeContent content={contentDataMap} sketchName={params.sketchName} />
      </Fragment>
    </div>
  );
};

Content.propTypes = {
  sketchContent: PropTypes.object,
  codeContent: PropTypes.string,
  match: PropTypes.object,
};
export default Content;
