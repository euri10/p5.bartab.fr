import React from "react";
import "../css/SideBar.css";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
const SideBar = ({ show, content }) => {
  let classSideBar = "sidebar";
  if (show) {
    classSideBar = "sidebar open";
  }
  return (
    <nav className={classSideBar}>
      {Object.entries(content).map(([key, value]) => {
        return (
          <div className="sidebar__entry" key={key}>
            <span className="sidebar__icon">{content[key].linkIcon}</span>
            <Link to={`/sketches/${key}`}>{key}</Link>
            <div className="sidebar__spacer"></div>
            <span className="sidebar__date">
              {content[key].date.toLocaleDateString()}
            </span>
          </div>
        );
      })}
    </nav>
  );
};
SideBar.propTypes = {
  show: PropTypes.bool,
  content: PropTypes.object,
};
export default SideBar;
