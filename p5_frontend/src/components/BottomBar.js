import React from "react";
import "../css/BottomBar.css";

const BottomBar = () => {
  return (
    <div className="bottombar">
      <a
        href="https://www.instagram.com/p5.bartab.fr"
        aria-label="Instagram profile"
      >
        
      </a>
      <a href="https://gitlab.com/euri10" aria-label="Gitlab profile">
        
      </a>
      <a href="https://github.com/euri10" aria-label="Github profile">
        
      </a>
      <a
        href="https://stackexchange.com/users/2259205/euri10"
        aria-label="StackExchange profile"
      >
        ﬊
      </a>
      <a
        href="https://stackoverflow.com/users/3581357/euri10"
        aria-label="StackOverflow profile"
      >
        
      </a>
    </div>
  );
};

export default BottomBar;
