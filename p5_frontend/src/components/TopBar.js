import React from "react";
import "../css/TopBar.css";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const TopBar = ({ sideBarClick }) => {
  return (
    <div className="topbar">
      <button
        className="topbar__hamburger"
        onClick={sideBarClick}
        aria-label="Open Sidebar"
      >
        
      </button>
      <Link className="topbar__sitename" to="/">
        p5.bartab.fr
      </Link>
      <div className="topbar__spacer"></div>
      <span className="topBar__about">
        <Link to="/about">About</Link>
      </span>
    </div>
  );
};

TopBar.propTypes = {
  sideBarClick: PropTypes.func,
};

export default TopBar;
