import PropTypes from "prop-types";
import React, { useEffect, useRef } from "react";

import p5 from "p5";
import generate from "shortid";
const Section = ({ sketchSrc }) => {
  const wrapperContainer = useRef(generate());
  const canvasRef = useRef(generate());
  useEffect(() => {
    let canvas = canvasRef.current;
    console.log(":: Section effect");
    async function loadSketch(skt) {
      const { default: sktfunc } = await import(`../sketches/${skt}`);
      canvas = new p5(sktfunc, wrapperContainer.current);
    }
    loadSketch(sketchSrc);

    return () => canvas.remove();
  }, [sketchSrc]);

  return (
    <div className="sketch__layout">
      <div ref={wrapperContainer}></div>
    </div>
  );
};

Section.propTypes = {
  sketchSrc: PropTypes.string,
};

export default Section;
