import PropTypes from "prop-types";
import React, { Fragment } from "react";
import "../css/SketchContent.css";
import Section from "./Section";

const SketchContent = ({ content, sketchName }) => {
  return (
    <Fragment>
      <div className="sketchcontent">
        <Section sketchSrc={content[sketchName].source} />
      </div>
    </Fragment>
  );
};

SketchContent.propTypes = {
  // content: PropTypes.object,
  match: PropTypes.object,
};

export default SketchContent;
