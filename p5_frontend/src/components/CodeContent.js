import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import "../css/CodeContent.css";
import SyntaxHighlighter from "react-syntax-highlighter";
import axios from "axios";

const CodeContent = ({ content, sketchName }) => {
  const [codeContentState, setCodeContentState] = useState("Loading");
  useEffect(() => {
    const fetchdata = async () => {
      const url = `./${content[sketchName].source}`;
      const result = await axios(url);
      setCodeContentState(result.data);
    };
    fetchdata();
  });
  return (
    <div className="codecontent">
      <SyntaxHighlighter language="javascript">
        {codeContentState}
      </SyntaxHighlighter>
    </div>
  );
};

CodeContent.propTypes = {
  content: PropTypes.object,
  match: PropTypes.object,
};
export default CodeContent;
