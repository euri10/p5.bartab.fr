import React from "react";
import "../css/About.css";

const About = (props) => {
  return (
    <div className="about">
      <span>Welcome to p5.bartab.fr</span>
      <br />
      <span>
        {/* eslint-disable-next-line react/no-unescaped-entities */}I wanted to
        learn some javascript  and reactjs , so here's an humble attempt at
        using it while doing some p5 art in the process.
      </span>
      <br />
      <span>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        99.99% of the time I'm coding backend stuff in python , yes, these{" "}
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        pieces of code you don't see that becomes ugly very fast...
      </span>
      <br />
      <span>
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        So I just hope this doesn't look like what I'm traditionnally working on
        !
      </span>
      <br />
      <span>
        All this website code is open-source{" "}
        <a href="https://gitlab.com/euri10/p5.bartab.fr/">here</a>.
      </span>
      <br />
      <span>
        Open an issue if you feel like I could do something differently, or if{" "}
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        it's real ugly !
      </span>
      <br />
      <span>Oh and btw I use </span>
    </div>
  );
};

export default About;
