import React from "react";
import { render, screen } from "@testing-library/react";
import "jest-canvas-mock";
import App from "../App";

it("renders topbar", () => {
  render(<App />);
  const headerTitle = screen.getByText("p5.bartab.fr");
  expect(headerTitle).toBeInTheDocument();
});
