export default function breathe(s) {
  const step_circle = 20;
  const circles_num = 15;
  const circles_xy = [...new Array(circles_num)];
  let numFrames = 60;
  s.setup = () => {
    s.createCanvas(400, 400);
    const startX = 200;
    const startY = 200;
    const mid = s.round(circles_num / 2) - 1;
    circles_xy.map((value, index, array) => {
      if (index < mid) {
        array[index] = {
          x: startX - index * step_circle,
          y: startY - index * step_circle,
        };
      } else {
        array[index] = {
          x: startX + index * step_circle,
          y: startY + index * step_circle,
        };
      }
      return array;
    });
  };

  s.draw = () => {
    const t = (1.0 * (s.frameCount - 1)) / numFrames;
    s.background(0);
    s.noFill(0);
    circles_xy.forEach((element, index, array) => {
      let we = element.x + element.x * s.sin(s.PI * t);
      let he = element.y + element.y * s.sin(s.PI * t);
      for (let i = 1; i < 12; i++) {
        s.stroke(255);
        s.strokeWeight(1);
        s.ellipse(
          element.x,
          element.y,
          we + i * step_circle,
          he + i * step_circle
        );
      }
    });
  };
}
