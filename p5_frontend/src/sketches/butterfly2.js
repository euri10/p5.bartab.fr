export default function butterfly2(s) {
  class ButterFly {
    constructor(radius, startX, startY) {
      this._radius = radius;
      this._startX = startX;
      this._startY = startY;
    }

    xb(theta) {
      return (
        s.sin(theta) *
        (s.exp(s.cos(theta)) -
          2 * s.cos(4 * theta) -
          s.pow(s.sin(theta / 12), 5))
      );
    }

    yb(theta) {
      return (
        s.cos(theta) *
        (s.exp(s.cos(theta)) -
          2 * s.cos(4 * theta) -
          s.pow(s.sin(theta / 12), 5))
      );
    }
    periodic(q) {
      return s.noise(
        SEED + RADIUS * s.cos(s.HALF_PI * q),
        RADIUS * s.sin(s.HALF_PI * q)
      );
    }
    offset(p) {
      // return 5.0 * s.pow(p, 3);
      return 3.0 * p;
    }
    display(t) {
      let p = this.periodic(t);
      for (let i = 0; i < DOTNUM; i++) {
        s.stroke(255, 255 - s.random(0, 55));
        s.strokeWeight(s.random(0, 2));
        s.point(
          this._startX + 2 * p * this._radius * this.xb(this.offset(i) + t),
          this._startY + this._radius * this.yb(this.offset(i) + t)
        );
      }
    }
  }

  const DOTNUM = 3000;
  let numFrames = 360;
  const butterfly = new ButterFly(50, 200, 150);
  const RADIUS = 2;
  const SEED = 1000;

  s.setup = () => {
    s.createCanvas(400, 400);
  };

  s.draw = () => {
    s.background(0);
    const t = (1.0 * (s.frameCount - 1)) / numFrames;
    butterfly.display(t);
  };
}
