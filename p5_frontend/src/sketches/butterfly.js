export default function butterfly(s) {
  class ButterFly {
    constructor(radius, startX, startY) {
      this._radius = radius;
      this._startX = startX;
      this._startY = startY;
    }

    xb(theta) {
      return (
        s.sin(theta) *
        (s.exp(s.cos(theta)) -
          2 * s.cos(4 * theta) -
          s.pow(s.sin(theta / 12), 5))
      );
    }

    yb(theta) {
      return (
        s.cos(theta) *
        (s.exp(s.cos(theta)) -
          2 * s.cos(4 * theta) -
          s.pow(s.sin(theta / 12), 5))
      );
    }
    display(t) {
      for (let i = 0; i < DOTNUM; i++) {
        s.point(
          this._startX + this._radius * this.xb(t + 10 * i),
          this._startY + this._radius * this.yb(t + 10 * i)
        );
      }
    }
  }

  const DOTNUM = 1000;
  let numFrames = 360;
  const butterfly = new ButterFly(50, 200, 150);

  s.setup = () => {
    s.createCanvas(400, 400);
  };

  s.draw = () => {
    s.background(0);
    const t = (1.0 * (s.frameCount - 1)) / numFrames;
    s.stroke(255, 250);
    butterfly.display(t);
  };
}
