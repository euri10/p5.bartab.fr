export default function cells(s) {
  const rows = 10;
  const columns = 10;
  let numFrames = 360;

  class Cell {
    constructor(tempX, tempY, tempW, tempH, tempAngle) {
      this._tempX = tempX;
      this._tempY = tempY;
      this._tempW = tempW;
      this._tempH = tempH;
      this._tempAngle = tempAngle;
    }

    oscillate() {
      this._tempAngle += 0.01;
    }

    display(t) {
      s.stroke(255);
      s.fill(125 + 125 * s.sin(this._tempAngle + 2 * s.PI * t));
      s.rect(this._tempX, this._tempY, this._tempW, this._tempH);
    }
  }

  const grid = [...new Array(rows)].map(() => [...new Array(columns)]);
  console.log("grid:", grid);
  s.setup = () => {
    console.log("rows:", rows);
    console.log("columns:", columns);
    s.createCanvas(400, 400);
    const wr = s.width / rows;
    console.log(wr);
    const hr = s.height / columns;
    console.log(hr);
    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < columns; j++) {
        grid[i][j] = new Cell(i * wr, j * hr, 1 * wr, 1 * hr, i + j);
      }
    }
    console.log(grid);
    // console.log('::: displayDensity:', s.displayDensity())
    // console.log('::: pixelDensity:', s.pixelDensity())
  };

  s.draw = () => {
    const t = (1.0 * (s.frameCount - 1)) / numFrames;
    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < columns; j++) {
        grid[i][j].oscillate();
        grid[i][j].display(t);
      }
    }
  };
}
