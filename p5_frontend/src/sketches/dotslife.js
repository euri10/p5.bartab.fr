export default function dotslife(s) {
  const DOTNUM = 500;
  const DOTCUBES = 1;
  let numFrames = 80;
  const dotsCubeArray = [...new Array(DOTCUBES)].map(() => [
    ...new Array(DOTNUM),
  ]);
  class DotsCube {
    constructor(xmin, xmax, ymin, ymax) {
      this._x = s.random(xmin, xmax);
      this._y = s.random(ymin, ymax);
      this._z = s.random(-10, 10);
      this._radius = s.random(2, 15);
      this._size = s.random(1, 5);
      this._offsetx = s.noise(this._x, -this._y);
      this._offsety = s.noise(-this._x, this._y);
      this._transparency = 255 - 5 * this._size;
    }
    easeInOutBack(x) {
      const c1 = 1.70158;
      const c2 = c1 * 1.525;
      return x < 0.5
        ? (s.pow(2 * x, 2) * ((c2 + 1) * 2 * x - c2)) / 2
        : (s.pow(2 * x - 2, 2) * ((c2 + 1) * (x * 2 - 2) + c2) + 2) / 2;
    }
    display(t, ytr) {
      s.stroke(255, this._transparency);
      s.strokeWeight(this._size);
      s.point(
        this._x + this._radius * s.cos(2 * s.PI * t + this._offsetx),
        this._y + this._radius * s.sin(2 * s.PI * t + this._offsety),
        this._z
      );
      s.translate(0, this.easeInOutBack(ytr));
    }
  }

  s.setup = () => {
    s.createCanvas(400, 400);
    for (let dn = 0; dn < DOTNUM; dn++) {
      dotsCubeArray[0][dn] = new DotsCube(100, 300, 100, 300);
      // dotsCubeArray[0][dn] = new DotsCube(50, 150, 50, 150);
      // dotsCubeArray[1][dn] = new DotsCube(250, 350, 50, 150);
      // dotsCubeArray[2][dn] = new DotsCube(50, 150, 250, 350);
      // dotsCubeArray[3][dn] = new DotsCube(250, 350, 250, 350);
    }
    console.log(dotsCubeArray[0][0]);
  };

  s.draw = () => {
    s.background(0);
    const t = (s.frameCount - 1) / numFrames;
    const ytr = s.map(s.sin(t), -1, 1, 0, 1);
    for (let cu = 0; cu < DOTCUBES; cu++) {
      for (let i = 0; i < DOTNUM; i++) {
        dotsCubeArray[cu][i].display(s.sin(t), ytr);
      }
    }
  };
}
