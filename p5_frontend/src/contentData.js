const contentDataMap = {
  template: {
    linkIcon: "✙",
    source: "sketch_template.js",
    date: new Date(2020, 8, 2),
  },
  cells: {
    linkIcon: "▦",
    source: "cells.js",
    date: new Date(2020, 8, 2),
  },
  breathe: {
    linkIcon: "☣",
    source: "breathe.js",
    date: new Date(2020, 8, 8),
  },
  dotslife: {
    linkIcon: "◎",
    source: "dotslife.js",
    date: new Date(2020, 8, 9),
  },
  butterfly: {
    linkIcon: "♃",
    source: "butterfly.js",
    date: new Date(2020, 8, 10),
  },
  butterfly2: {
    linkIcon: "ȥ",
    source: "butterfly2.js",
    date: new Date(2020, 8, 24),
  },
  // REMEMBER date month begins at fucking 0 !!
};
export default contentDataMap;
