import React, { Fragment, useState } from "react";
import { Route, BrowserRouter, Routes } from "react-router-dom";
import About from "./components/About";
import Backdrop from "./components/Backdrop";
import BottomBar from "./components/BottomBar";
import Content from "./components/Content";
import Home from "./components/Home";
import SideBar from "./components/SideBar";
import TopBar from "./components/TopBar";
import "./css/App.css";
import contentDataMap from "./contentData";

function App() {
  const [sideBarOpen, setSideBarOpen] = useState(false);
  const sideBarClick = () => {
    setSideBarOpen(!sideBarOpen);
  };
  const backdropClick = () => {
    setSideBarOpen(false);
  };
  return (
    <BrowserRouter>
      <Fragment>
        <TopBar sideBarClick={sideBarClick} />
        <SideBar show={sideBarOpen} content={contentDataMap} />
        {sideBarOpen ? <Backdrop backdropClick={backdropClick} /> : null}
        <Routes>
          <Route path={"/"} element={<Home />} />
          <Route path={"/about"} element={<About />} />
          <Route path={"/sketches/:sketchName"} element={<Content />} />
        </Routes>
        <BottomBar />
      </Fragment>
    </BrowserRouter>
  );
}

export default App;
