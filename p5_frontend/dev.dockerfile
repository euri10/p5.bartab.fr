FROM node:16.13.0
# set working directory
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY --chown=node:node package.json ./
COPY --chown=node:node package-lock.json ./
RUN npm install
# add app
COPY --chown=node:node . ./
USER node

# start app
#CMD ["bash", "-c", "while true; do sleep 1; done"]
CMD ["npm", "start"]